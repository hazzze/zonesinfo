package zones;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.datacontract.schemas._2004._07.zu_zonesws.ArrayOfZoneInfo;
import zones.ZoneInfo.Document;

/**
 * @author Alfie
 */
@ManagedBean
@RequestScoped
public class Zones {
//    org.tempuri.ZuZones service = new org.tempuri.ZuZones();
//    org.tempuri.IZuZones port = service.getBasicHttpBindingIZuZones();

    private List<ZoneInfo> ZoneInfo = new ArrayList<ZoneInfo>();
    private String objNumber = "02:30:060302:45";  // Тестовая зона по умолчанию

    /**
     * Загрузка страницы по земельным участкам
     *
     * @return plots.xhtml
     */
    public String showPlots() {
        return "plots?faces-redirect=true";
    }

    /**
     * Загрука страницы по участкам
     *
     * @return zones.xhtml
     */
    public String showZones() {
        return "zones?faces-redirect=true";
    }

    /**
     * Загрузка данных по запросу
     *
     * @param objNumber - номер зоны / земельного участка
     */
    public void updateZones() {
        if (objNumber.length() > 0) {
            ArrayOfZoneInfo zones = getDataJSON(objNumber);
            SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
            Date dateDF = new Date();
            ZoneInfo.clear();
            for (int i = 0; i < zones.getZoneInfo().size(); i++) {
                ZoneInfo zoneinfo = new ZoneInfo();
                if (zones.getZoneInfo().get(i).getDocuments().getValue() != null) {
                    for (int d = 0; d < zones.getZoneInfo().get(i).getDocuments().getValue().getDocument().size(); d++) {
                        ZoneInfo.Document doc = zoneinfo.new Document(new Date(), "", "");
                        //ZoneInfo.Document doc = new ZoneInfo.Document(new Date(), "", ""); // Для static класса
                        try {   // На случай ошибки при парсинге DocumentDate
                            dateDF = DF.parse(zones.getZoneInfo().get(i).getDocuments().getValue().getDocument().get(d).getDocumentDate().getValue().toString());
                            doc.setDocumentDate(dateDF);
                        } catch (ParseException ex) {
                            ex.printStackTrace();
                        }
                        doc.setDocumentName(zones.getZoneInfo().get(i).getDocuments().getValue().getDocument().get(d).getDocumentName().getValue() == null ? "" : zones.getZoneInfo().get(i).getDocuments().getValue().getDocument().get(d).getDocumentName().getValue());
                        doc.setDocumentNumber(zones.getZoneInfo().get(i).getDocuments().getValue().getDocument().get(d).getDocumentNumber().getValue() == null ? "" : zones.getZoneInfo().get(i).getDocuments().getValue().getDocument().get(d).getDocumentNumber().getValue());
                        zoneinfo.getDocument().add(doc);
                    }
                }
                if (zones.getZoneInfo().get(i).getEncumbrances().getValue() != null) {
                    for (int e = 0; e < zones.getZoneInfo().get(i).getEncumbrances().getValue().getString().size(); e++) {
                        zoneinfo.getEncumbrances().add(zones.getZoneInfo().get(i).getEncumbrances().getValue().getString().get(e));
                    }
                }
                if (zones.getZoneInfo().get(i).getParcels().getValue() != null) {
                    for (int p = 0; p < zones.getZoneInfo().get(i).getParcels().getValue().getString().size(); p++) {
                        zoneinfo.getParcels().add(zones.getZoneInfo().get(i).getParcels().getValue().getString().get(p));
                    }
                }
                try {   // На случай ошибки при парсинге ServitutDate
                    dateDF = DF.parse(zones.getZoneInfo().get(i).getServitutDate().getValue().toString());
                    zoneinfo.setServitutDate(dateDF);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                try {   // На случай ошибки при парсинге ZoneDate
                    dateDF = DF.parse(zones.getZoneInfo().get(i).getZoneDate().getValue().toString());
                    zoneinfo.setZoneDate(dateDF);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                try {   // На случай ошибки при парсинге ZuDate
                    dateDF = DF.parse(zones.getZoneInfo().get(i).getZuDate().getValue().toString());
                    zoneinfo.setZuDate(dateDF);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
                zoneinfo.setServitut(zones.getZoneInfo().get(i).getServitut().getValue() == null ? "" : zones.getZoneInfo().get(i).getServitut().getValue());
                zoneinfo.setZoneNumber(zones.getZoneInfo().get(i).getZoneNumber().getValue() == null ? "" : zones.getZoneInfo().get(i).getZoneNumber().getValue());
                ZoneInfo.add(zoneinfo);
            }
        }
    }

    /**
     * Запрос
     *
     * @param objectNumber - номер зоны / земельного участка
     * @return - ArrayOfZoneInfo
     */
    private static ArrayOfZoneInfo getDataJSON(java.lang.String objectNumber) {
        org.tempuri.ZuZones service = new org.tempuri.ZuZones();
        org.tempuri.IZuZones port = service.getBasicHttpBindingIZuZones();
        return port.getDataJSON(objectNumber);
    }
    
    /**
     * Извлекаем необходимый текст между кавычками
     * @param str - вся строка
     * @return - строка, что была внутри кавычек
     */
    public String betweenQuout(String str) {
        String s = "";
        Pattern p = Pattern.compile("\"(.*?)\"");
        Matcher m = p.matcher(str);
        if (m.find()) {
            s = m.group();
        }
        return s;
    }
    
    /**
     * Преобразовываем дату в нужный нам вид
     * @param date - дата в стандартном формате
     * @return дата в строковом виде нужного формата
     */
    public String DateStr(Date date) {
        String year = Integer.toString(date.getYear() + 1900),
                month = Integer.toString(date.getMonth() + 1),
                day = Integer.toString(date.getDate()),
                monthStr = "";
        switch (date.getMonth() + 1) {
            case 1:
                monthStr = "января";
                break;
            case 2:
                monthStr = "февраля";
                break;
            case 3:
                monthStr = "марта";
                break;
            case 4:
                monthStr = "апреля";
                break;
            case 5:
                monthStr = "мая";
                break;
            case 6:
                monthStr = "июня";
                break;
            case 7:
                monthStr = "июля";
                break;
            case 8:
                monthStr = "августа";
                break;
            case 9:
                monthStr = "сентября";
                break;
            case 10:
                monthStr = "октября";
                break;
            case 11:
                monthStr = "ноября";
                break;
            case 12:
                monthStr = "декабря";
                break;
        }
        return day + " " + monthStr + " " + year + "г.";
    }

    public Zones() {
    }

    public List<ZoneInfo> getZoneInfo() {
        return ZoneInfo;
    }

    public void setZoneInfo(List<ZoneInfo> ZoneInfo) {
        this.ZoneInfo = ZoneInfo;
    }

    public String getObjNumber() {
        return objNumber;
    }

    public void setObjNumber(String objNumber) {
        this.objNumber = objNumber;
    }

}
