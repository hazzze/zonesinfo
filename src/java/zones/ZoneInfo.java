/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zones;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Alfie
 */
public class ZoneInfo {

    private List<Document> Document = new ArrayList<>();
    private List<String> Parcels = new ArrayList<>();
    private List<String> Encumbrances = new ArrayList<>();
    private Date ServitutDate = new Date();
    private Date ZoneDate = new Date();
    private Date ZuDate = new Date();
    private String ZoneNumber = "";
    private String Servitut = "";

    public class Document {

        Date DocumentDate = new Date();
        String DocumentName = "";
        String DocumentNumber = "";

        public Document(Date DocumentDate, String DocumentName, String DocumentNumber) {
            this.DocumentDate = DocumentDate;
            this.DocumentName = DocumentName;
            this.DocumentNumber = DocumentNumber;
        }

        public Date getDocumentDate() {
            return DocumentDate;
        }

        public void setDocumentDate(Date DocumentDate) {
            this.DocumentDate = DocumentDate;
        }

        public String getDocumentName() {
            return DocumentName;
        }

        public void setDocumentName(String DocumentName) {
            this.DocumentName = DocumentName;
        }

        public String getDocumentNumber() {
            return DocumentNumber;
        }

        public void setDocumentNumber(String DocumentNumber) {
            this.DocumentNumber = DocumentNumber;
        }
    }

    public List<Document> getDocument() {
        return Document;
    }

    public void setDocument(List<Document> Document) {
        this.Document = Document;
    }
    
    public List<String> getParcels() {
        return Parcels;
    }

    public void setParcels(List<String> Parcels) {
        this.Parcels = Parcels;
    }

    public List<String> getEncumbrances() {
        return Encumbrances;
    }

    public void setEncumbrances(List<String> Encumbrances) {
        this.Encumbrances = Encumbrances;
    }

    public Date getServitutDate() {
        return ServitutDate;
    }

    public void setServitutDate(Date ServitutDate) {
        this.ServitutDate = ServitutDate;
    }

    public Date getZoneDate() {
        return ZoneDate;
    }

    public void setZoneDate(Date ZoneDate) {
        this.ZoneDate = ZoneDate;
    }

    public Date getZuDate() {
        return ZuDate;
    }

    public void setZuDate(Date ZuDate) {
        this.ZuDate = ZuDate;
    }

    public String getZoneNumber() {
        return ZoneNumber;
    }

    public void setZoneNumber(String ZoneNumber) {
        this.ZoneNumber = ZoneNumber;
    }

    public String getServitut() {
        return Servitut;
    }

    public void setServitut(String Servitut) {
        this.Servitut = Servitut;
    }
    
}
