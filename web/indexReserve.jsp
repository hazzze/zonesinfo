<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : index
    Created on : 04.07.2016, 14:21:56
    Author     : Alfie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Информация по зонам и земельным участкам</title>
    </head>
    <body>
        <ul>
            <li style="list-style-type: none" ><a href="zones.do"><img title="Зоны"/>Зоны</a></li>
            <li style="list-style-type: none" ><a href="plots.do"><img title="Земельные участки"/>Участки</a></li>
        </ul>
    </body>
</html>
